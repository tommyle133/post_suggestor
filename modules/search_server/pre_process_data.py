from pymongo import MongoReplicaSetClient, MongoClient
from pymongo.errors import AutoReconnect
import requests
import logging
import datetime

from normalize_utils import normalize_price
from query_analyzer import normalize_tags

model_url = "http://localhost:5000/api/v1/real-estate-extraction"
database_url = "localhost:27017"
logging.basicConfig(level=logging.DEBUG)

db = MongoClient(database_url)["real-estate"]
coll = db["data_crawler"]
resDB = db["data_post_norm"]

contents = []
reCol = []

def read():
    data = []
    empcol = coll.find().hint([('$natural', 1)])
    for emp in empcol:
        contents.append(emp["content"])
        data.append(emp)
    return data

reCol = read()
norm_tags = []
newData=[]
total = 101500
for i in range(0, total, 100):
    newData.extend(reCol[i:i+100])
    res = requests.post(model_url, json=contents[i:i+100]).json()
    raw_tags = []
    for item in res:
        raw_tag = item['tags']
        raw_tags.append(raw_tag)
    tags = []

    k = 0
    for item in raw_tags:
        tag = {}
        for chunk in item:
            t = chunk['type']
            c = chunk['content']
            if t == 'normal':
                continue
            if tag.get(t) is None:
                tag[t] = []
            c = c.lower().strip()
            tag[t].append(c)
        tags.append(tag)
        newData[i+k]['tags']=tag
        k+=1

    k = 0
    for item in tags:
        norm_tag = normalize_tags(item)
        norm_tags.append(norm_tag)
        newData[i+k]['norm_val']=norm_tag
        newData[i+k]['index_val'] = norm_tag
        k+=1

resDB.insert_many(newData)

